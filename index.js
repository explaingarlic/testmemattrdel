const { syncBuiltinESMExports } = require("module");
const process = require("process");

function logMem(when) {
    console.log(Math.round(process.memoryUsage().heapTotal / 1000000, 2).toString() + "mb Used " + when);
}

if(!global.gc) {
    console.log("Please use the --expose-gc flag and make sure your node installation supports direct garbage collector calls.")
    process.exit(1);
}



let godObject = {};
const allocatingHowManyNumbers = Math.floor(Math.random() * 1000000) + 5000000

function start() {
    console.log("Test suite of effectiveness of delete keyword.\n")
    console.log("Test #0 - seeing how much memory is taken up 5+ seconds after startup.");
    logMem("before waiting 5 seconds.")
    setTimeout(() => {
        logMem("after waiting 5 seconds.")
        stageOne();
    },5000)
}
function stageOne() { 
    console.log("\nTest #1 - Seeing if/when deletion triggers, roughly.\n")
    console.log("We are allocating", allocatingHowManyNumbers, "numbers.")
    logMem("just before allocation")
    
    for(let i = 0; i < allocatingHowManyNumbers; i++) {
        const key = Math.random().toString() + "-" + Math.random().toString();
        if(!godObject[key]) {
            godObject[key] = Math.random();
        }
    }
    logMem("after allocation.");
    
    for(let key in godObject) {
        if(Math.random() < 0.99) delete godObject[key];
    }
    
    logMem("after deletion.");
    let i = 0;
    const interval = setInterval(()=> {
        logMem(i + " milliseconds after deletion.")
        i += 1000;
        if(i > 5000) stageTwo(interval);
    }, 1000);
}

function stageTwo(interval) {
    console.log("\nTest #2 - checking key count, first manual GC call...\n")
    clearInterval(interval);
    console.log("There are", Object.keys(godObject).length, "keys still allocated.")
    logMem("after calculating keys, before calling garbage collector manually...")
    global.gc()
    logMem("after calling garbage collector manually.")
    stageThree();
}

function stageThree() {
    console.log("\nTest #3 - waiting 5 seconds, logging memory after+before calling GC again\n")
    setTimeout(()=>{
        logMem("before second gc call, 5+ seconds after original")
        global.gc()
        logMem("after second gc call.")
    }, 5000)
}

start();
