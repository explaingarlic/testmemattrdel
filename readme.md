# Testing `delete` Keyword in Node.JS


I am curious as to how the delete keyword works - whether it immediately clears memory that has been deleted if the refcount is kept low.

One way to find out is to check the unreadable and hard to traverse source code of the V8 engine. 

Another way is to just do this in 15 minutes :-)

The plan is to setup this repo with ci/cd stuff until a significant portion of different nodejs environments is covered